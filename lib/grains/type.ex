defmodule Grains.Type do

  alias Grains.{StatefulMap, CounterAgent}

  @nr_types 4
  @nr_colors 255
  @nr_saturations 50

  def select_type(uuid) do
    Integer.mod(parse(uuid), @nr_types)
  end


  def select_count(uuid) do
    nr = StatefulMap.get(uuid)
    nr = if nr == nil do
      nr = CounterAgent.click()
      StatefulMap.put(uuid, nr)
      nr
    else
      nr
    end
    nr
  end


  # returns a tuple for hue / saturation
  def select_color(uuid) do
    i = parse(uuid)
    h = Integer.mod(i, @nr_colors);
    s = 50 + Integer.mod(i, @nr_saturations);
    {h, s}
  end



  def parse(uuid) do
    {i, _} = Integer.parse(uuid, 16)
    i
  end



end
