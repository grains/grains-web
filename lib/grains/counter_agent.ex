defmodule Grains.CounterAgent do
  def start_link() do
    Agent.start_link(fn -> 0 end, name: __MODULE__)
  end

  def click() do
    Agent.get_and_update(__MODULE__, fn(n) -> {n + 1, n + 1} end)
  end

  def set(new_value) do
    Agent.update(__MODULE__, fn(_n) -> new_value end)
  end

  def get() do
    Agent.get(__MODULE__, fn(n) -> n end)
  end
end
