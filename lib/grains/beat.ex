defmodule Grains.Beat do

  use GenServer
  require Logger

  @minute 60_000

  def start_link(bpm), do: GenServer.start_link(__MODULE__, bpm, name: __MODULE__)


  def set_bpm(bpm), do: GenServer.cast(__MODULE__, {:set_bpm, bpm})

  def get_bpm(), do: GenServer.call(__MODULE__, :get_bpm)

  def init(bpm) do
    millisec = round(@minute / bpm)
    tick(millisec)
    {:ok, millisec}
  end

  defp tick(millisec), do: Process.send_after(self(), :tick, millisec)

  def handle_cast({:set_bpm, bpm}, _state) do
    {:noreply, round(@minute / bpm) }
  end

  def handle_call(:get_bpm, _from, state) do
    {:reply, round(@minute / state), state}
  end


  def handle_info(:tick, state) do
    send_clock()
    Grengine.Conductor.perform()
    |> send_values

    tick(state)

    {:noreply, state}
  end

  def handle_info(_, state), do: {:noreply, state}

  def send_clock do
    Grains.Endpoint.broadcast("osc", "clock", %{})
  end

  def send_values(values) do
    Logger.info "sending performance values"
    Grains.Endpoint.broadcast("osc", "slider", values)
  end


end
