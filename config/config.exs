# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration

# Configures the endpoint
config :grains, Grains.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "jsJ90HAYIQUhsnJg0jl3FV7mGz36aJUWs4/yrU9fo/KPSKIgReB6SzJtOKPuyLkV",
  render_errors: [view: Grains.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Grains.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"

# This line was automatically added by ansible-elixir-stack setup script
if System.get_env("SERVER") do
  config :phoenix, :serve_endpoints, true
end
