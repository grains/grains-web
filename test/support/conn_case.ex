defmodule Grains.ConnCase do
  @moduledoc """
  This module defines the test case to be used by
  tests that require setting up a connection.

  Sucansaction which is reset at the beginning
  of the test unless the test case is marked as async.
  """

  use ExUnit.CaseTemplate

  using do
    quote do
      # Import conveniences for testing with connections
      use Phoenix.ConnTest

      import Grains.Router.Helpers

      # The default endpoint for testing
      @endpoint Grains.Endpoint
    end
  end

  setup tags do
{:ok, conn: Phoenix.ConnTest.build_conn()}
  end
end
