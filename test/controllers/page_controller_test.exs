defmodule Grains.PageControllerTest do
  use Grains.ConnCase

  test "GET /", %{conn: conn} do
    conn = get conn, "/"
    assert html_response(conn, 200) =~ "Welcome to Grains!"
  end
end
