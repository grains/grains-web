defmodule Grains.ValueView do

  use Grains.Web, :view

  def render("value.json", %{value: {key, value}}) do

    %{ element: key,
       value: value }
  end
end
