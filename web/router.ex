defmodule Grains.Router do
  use Grains.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug Grains.Token
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :conductor_layout do
    plug :put_layout, {Grains.LayoutView, :conductor}
  end

  pipeline :play_layout do
    plug :put_layout, {Grains.LayoutView, :play}
  end

  scope "/", Grains do
    pipe_through :browser # Use the default browser stack


    get "/grains/:grain", GrainsController, :show
    get "/", PageController, :index
  end

  scope "/play", Grains do
    pipe_through [:browser, :play_layout]

    get "/", GrainsController, :index
    get "/play", GrainsController, :play
  end

  scope "/conductor", Grains do
    pipe_through [:browser, :conductor_layout]

    get "/", ConductorController, :index
  end





  # Other scopes may use custom stacks.
  # scope "/api", Grains do
  #   pipe_through :api
  # end
end
