defmodule Grains.PageController do
  use Grains.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end

end
