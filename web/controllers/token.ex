defmodule Grains.Token do
  import Plug.Conn

  alias Grains.Type

  def init(opts) do
    opts
  end


  def call(conn, _opts) do
    uuid = get_session(conn, :user_uuid) || UUID.uuid4(:hex)
    token = Phoenix.Token.sign(conn, "user socket", uuid )
    type = Type.select_type(uuid)
    {hue, saturation} = Type.select_color(uuid)
    nr = Type.select_count(uuid)
    IO.puts "User Type: #{type}, Nr: #{nr}"

    conn
    |> assign(:user_token, token)
    |> assign(:user_type, type)
    |> assign(:user_hue, hue)
    |> assign(:user_name, nr)
    |> assign(:user_saturation, saturation)
    |> put_session(:user_uuid, uuid)
    |> configure_session(renew: true)

  end

end
