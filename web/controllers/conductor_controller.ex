defmodule Grains.ConductorController do

  use Grains.Web, :controller

  def index(conn, _params) do
    bpm = Grains.Beat.get_bpm
    render conn, "index.html", bpm: bpm
  end

end
