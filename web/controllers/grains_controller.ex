defmodule Grains.GrainsController do
  use Grains.Web, :controller

  def index(conn, _) do
    render conn, "index.html"
  end

  def play(conn, _) do
    render conn, "play.html"
  end

  def show(conn, %{"grain" => grain}) do
    render conn, "#{grain}.html", grain: grain
  end

end
