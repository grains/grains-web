let Counter = {

  msgs: 0,
  msgs_sec: 0,
  msgs_last: 0,

  timer: {},
  msgElem: {},

  set_nr_messages(nr) {
    this.msgs = nr;
    this.msgElem.innerHTML = nr;
  },

  calculate() {
    let mps = (this.msgs - this.msgs_last);
    this.msgs_last = this.msgs;
    this.mpsElem.innerHTML = mps;
  },

  unmount() {

    clearInterval(this.timer);
  },

  init(domId) {

    if (!document.getElementById(domId)) {
      return;
    }

    this.msgElem = document.getElementById(domId + "_msg");
    this.mpsElem = document.getElementById(domId + "_mps");

    this.timer = setInterval(function() { Counter.calculate(); }, 1000);



  }


}

export default Counter;
