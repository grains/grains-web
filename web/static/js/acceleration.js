import Interface from "interface.js";
import _ from "underscore";

let AccelerationPanel = {

    accelerationChannel : null,

    sliders: {},
    slider_titles: ["z"],
    old_values: [0],

    init(socket, domId) {

        if (!document.getElementById(domId)) {
            return;
        }


        let that = this;
        socket.connect();
        this.accelerationChannel = socket.channel("acceleration:" + domId);


        this.accelerationChannel.join()
            .receive("ok", resp => {
                // resp.values.forEach( (element) => {
                //    this.setSliderValue(element);
                // } );
            })
            .receive("error", reason => console.log("join failed", reason));


        let a = new Interface.Panel({
            container:document.getElementById(domId)
        });
        console.log("adding acceleration");
        this.orientation = new Interface.Accelerometer({
          onvaluechange: function(_x, _y,z) {
            that.sliders['z'].setValue(z);

            that.send_value([z]);
            }

        }).start();

        let nrSliders = this.slider_titles.length;

        this.slider_titles.forEach( (s, idx) => {
          this.sliders[s] = new Interface.Slider({
            bounds: [(1/nrSliders) * idx, .1, 1 / nrSliders, .5],
            label: s,

          });
          // a.add(this.sliders[s]);
        });

      a.background = 'black';
      console.log("initialized acceleration");
    },

    send_value(values) {
      let scaled = values.map(function(i) {
        // scale to +/- 3 to one decimal
        return Math.round( 60 * (i- 0.5) ) / 10;
      });
      if (!_.isEqual(scaled, this.old_values)) {
        let payload = {z: scaled[0]};
        if (scaled[0] > 0) {
          this.accelerationChannel.push("acceleration", payload)
            .receive("error", e => console.log(e));
        }
        this.old_values = scaled;
      }



    },

};

export default AccelerationPanel;
