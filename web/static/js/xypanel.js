
import Interface from "interface.js";
import _ from "underscore";

let XyPanel = {

  xy: {},
  xyChannel : null,

  old_x: 0,
  old_y: 0,
  dot_id: 'na',
  timer: {},
  initalized: false,
  interval: 30,
  types: {0: 'circle', 1: 'square', 2: 'triangle', 3: 'star' },

  init(socket, domId, width) {

    let element = document.getElementById(domId);
    if (!element) {
      return;
    }



    let that = this;
    socket.connect();
    this.xyChannel = socket.channel("xy:all");


    this.xyChannel.join()
      .receive("ok", resp => {
        if (this.initialized) { return true; }
        this.initialized = true;

        console.log("join xy: ", resp.dot_id);
        that.dot_id = resp.dot_id;
        that.shape = that.types[resp.shape] || 'circle';

        var a = new Interface.Panel({  background:"#000", container: element });
        that.xy = new Interface.XY({
          childWidth: 45,
          numChildren: 1,
          childIds: [that.dot_id],
          shapes: [that.shape],
          background:"#111",
          fill: "rgba(127,127,127,.2)",
          bounds:[0,0,1,1],
        });
        // send regular timed data
        that.timer = setInterval( function() { that.send_value(that.xy.values); }, that.interval);

        a.background = 'black';
        a.add(that.xy);
      })
      .receive("error", reason => console.log(reason));
    this.xyChannel.on('interval', state => {
      that.interval = parseInt(1000 / state.interval);
      clearInterval(that.timer);
      that.timer = setInterval( function() { that.send_value(that.xy.values); }, that.interval);

    });
    element.style.width = width;
    element.style.height = width;

    console.log("adding xy");



  },

  send_value(values) {

    let x = Math.round(200 * (values[0].x - 0.5) * 3.14) / 100;
    let y = Math.round(200 * (values[0].y - 0.5) * 3.14) / 100;

    let payload = {x: x,
                   y: y
                  };
    this.xyChannel.push("xy", payload)
      .receive("error", e => console.log("foo"));



  },

  stopTimer() {
    clearInterval(this.timer);
  },

  setInterval(interval) {
    this.interval = interval;
  }



};

export default XyPanel;
