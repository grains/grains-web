import Interface from "interface.js";

let BpmPanel = {

  sliderChannel : null,
  bpmLabel: null,
  bpmSlider: null,
  intervalLabel: null,
  intervalSlider: null,



  init(socket, domId) {
    if (!document.getElementById(domId)) {
      console.log("No element found: " + domId);
      return;
    }


    let that = this;
    socket.connect();
    this.sliderChannel = socket.channel("conductor:" + domId);

    this.sliderChannel.join()
      .receive("ok", response => {
        that.bpmLabel.setValue(response.bpm);
        that.bpmSlider.setValue(response.bpm);
      })
      .receive("error", reason => console.log("join failed", reason));

    this.sliderChannel.on("set_bpm", response =>{
      that.bpmLabel.setValue(response.bpm);
      console.log("set bpm", response.bpm);
    });

    this.sliderChannel.on("set_interval", response =>{
      that.intervalLabel.setValue(response.interval);
      console.log("set interval", response.interval);
    });

    let a = new Interface.Panel({
      container:document.getElementById(domId)
    });


    this.bpmLabel = new Interface.Label({
      bounds: [.8, .1, .2, .9],
      hAlign: 'center',
      value: '0',
      size: 28,
      style: 'bold'
    });

    this.bpmSlider = new Interface.Slider({
      bounds: [.05, .05, .7, 0.9],
      label: 'BPM',
      min: 30,
      max: 180,
      isVertical: false,
      onvaluechange: function() {
        that.send_value(this.value);
      }

    });

    // a.add(this.bpmSlider, this.bpmLabel);


    this.intervalLabel = new Interface.Label({
      bounds: [.8, .1, .2, .9],
      hAlign: 'center',
      value: '0',
      size: 28,
      style: 'bold'
    });

    this.intervalSlider = new Interface.Slider({
      bounds: [.05, .05, .7, 0.9],
      label: 'interval',
      min: 1,
      max: 30,
      isVertical: false,
      onvaluechange: function() {
        that.send_interval(this.value);
      }

    });

    a.add(this.intervalSlider, this.intervalLabel);


    a.background = 'black';
    console.log("initialized bpm");
  },

  send_value(value) {
    let payload = {slider: 'bpm', value: value};
    this.sliderChannel.push("master", payload)
      .receive("error", e => console.log(e));

  },
  send_interval(value) {
    console.log("send_interval", value);
    let payload = {slider: 'interval', value: value};
    this.sliderChannel.push("master", payload)
      .receive("error", e => console.log(e));

  },


};

export default BpmPanel;
