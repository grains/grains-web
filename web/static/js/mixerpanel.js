import Interface from "interface.js";

let MixerPanel = {

  presence: {},

  sliders: {},
  buttons: {},

  init(socket, domId, sliders, presence) {
    if (!document.getElementById(domId)) {
      console.log("No element found: " + domId);
      return;
    }


    let that = this;
    console.log(presence);
    this.presence = presence;


    let a = new Interface.Panel({
      container:document.getElementById(domId)
    });

    let nrSliders = sliders.length;

    sliders.forEach( (s, idx) => {
      this.sliders[s] = new Interface.Slider({
        bounds: [(1/nrSliders) * idx, .1, 1 / nrSliders, .5],
        label: s,

        onvaluechange: function() {
          that.presence.set_volume(s, this.value);
        }
      });
      this.buttons[s] = new Interface.Button({
        bounds: [(1/nrSliders) * idx, .6, 1 / nrSliders, .1],
        label:'Abs',
        onvaluechange: function() {
          that.presence.set_absolute(s, this.value);
        }

      });


      console.log(s);
      a.add(this.sliders[s]);
      a.add(this.buttons[s]);
    });



    a.background = 'black';
    console.log("initialized slider");
  }



};

export default MixerPanel;
