import Interface from "interface.js";
import _ from "underscore";

let OrientationPanel = {

  orientationChannel : null,

  sliders: {},
  slider_titles: ["pitch", "roll"],
  old_values: [0,0],
  min_pitch: 99,
  max_pitch: -99,
  min_roll: 99,
  max_roll: -99,
  scale_pitch: 1,
  scale_roll: 1,

  calibrate(pitch, roll) {

    this.min_pitch = Math.min(pitch, this.min_pitch);
    this.max_pitch = Math.max(pitch, this.max_pitch);
    this.min_roll = Math.min(roll, this.min_roll);
    this.max_roll = Math.max(roll, this.max_roll);

    this.scale_pitch = (this.max_pitch - this.min_pitch);
    this.scale_roll = (this.max_roll - this.min_roll);

  },

  init(socket, domId) {

    if (!document.getElementById(domId)) {
      return;
    }


    let that = this;
    socket.connect();
    this.orientationChannel = socket.channel("orientation:" + domId);


    this.orientationChannel.join()
      .receive("ok", resp => {
        // resp.values.forEach( (element) => {
        //    this.setSliderValue(element);
        // } );
      })
      .receive("error", reason => console.log("join failed", reason));

    let a = new Interface.Panel({
      container: document.getElementById(domId)
    });
    console.log("adding orientation");
    this.orientation = new Interface.Orientation({
      onvaluechange: function(pitch, roll, _yaw, _heading) {
        pitch = pitch - 0.5; // beacuse 1 is straigh up
        roll = roll - 0.5;

        that.sliders['pitch'].setValue(pitch);
        that.sliders['roll'].setValue(roll);


        that.calibrate(pitch, roll);

        let shape = document.getElementById("shape");

        let x = 100 * (roll - that.min_roll)  / that.scale_roll;
        let y = 100 * (pitch - that.min_pitch) / that.scale_pitch;

        shape.setAttribute('transform','translate(' + (x) + ', ' + (y) + ')');

        that.send_value([x, y]);
      }

    }).start();

    let nrSliders = this.slider_titles.length;

    this.slider_titles.forEach( (s, idx) => {
      this.sliders[s] = new Interface.Slider({
        bounds: [(1/nrSliders) * idx, .1, 1 / nrSliders, .5],
        label: s,


      });
      console.log(s);
      //            a.add(this.sliders[s]);
    });

    a.background = 'black';
    console.log("initialized orientation");
  },

  send_value(values) {

    // values[0] = (((values[0] - 0.25) / 0.75) - 0.5) * 2;  // scale to normal movement range
    // values[1] = Math.pow((values[1] - 0.5), 3) * 8;  // prefer larger amounts

    // console.log(values);
    let scaled = values.map(function(i) {
      // scale to +/- Pi to one decimal
      i = (i/100.0) - 0.5;
      // i = Math.pow(i, 3);
      return Math.round(20 * i * Math.PI) / 10;   // parseInt(i * 127);
    });
    // console.log(scaled);
    if (!_.isEqual(scaled, this.old_values)) {
      let payload = {pitch: scaled[1],
                     roll: scaled[0]
                    };

      this.orientationChannel.push("orientation", payload)
        .receive("error", e => console.log(e));
      this.old_values = scaled;
    }



  },

};

export default OrientationPanel;
