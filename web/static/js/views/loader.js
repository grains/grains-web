import MainView    from './main';
import ConductorIndexView from './conductor';
import GrainsPlayView from './player';

// Collection of specific view modules
const views = {
  ConductorIndexView,
  GrainsPlayView
};

export default function loadView(viewName) {
  return views[viewName] || MainView;
}
