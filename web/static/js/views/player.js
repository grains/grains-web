import MainView from "./main";

import socket from "../socket";
import SliderPanel from "../slider";
import OrientationPanel from "../orientation";
import AccelerationPanel from "../acceleration";
import XyPanel from "../xypanel";

export default class View extends MainView {
  mount() {
    super.mount();
    let width = screen.width;

    XyPanel.init(socket, "xyPanel", width);
//    OrientationPanel.init(socket, "orientationPanel");
    AccelerationPanel.init(socket, "accelerationPanel");




    console.log("PlayerView mounted");
  }

  unmount() {
    super.unmount();
    XyPanel.stopTimer();

    console.log("PlayerView unmounted");
  }


}
