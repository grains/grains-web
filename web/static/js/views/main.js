
import {Socket, Presence} from "phoenix"


import socket from "../socket";


let formatTimestamp = (timestamp) => {
  let date = new Date(timestamp);
  return date.toLocaleDateString();
}

let listBy = (user, {metas: metas}) => {
  return {
    user: user,
    onlineAt: formatTimestamp(metas[0].online_at)
  };
}

export default class MainView {

  constructor() {
    this._presences = {};
  }


  mount() {
    // This will be executed when the document loads..
    let room = socket.channel("presence:lobby", {});

    room.on("presence_state", state => {
      console.log("p_s");
      this._presences = Presence.syncState(this._presences, state);
      this.render(this._presences);
    });

    room.on("presence_diff", diff => {
      console.log("p_d");

      console.log(diff);
      this._presences = Presence.syncDiff(this._presences, diff);
      this.render(this._presences, diff);
    });

    console.log("joining presence");

    room.join().receive("ok", resp => {
      console.log("presence joined");
    })
      .receive("error", reason => console.log("presence join failed", reason));


    console.log('MainView mounted');
  }

  unmount() {
    // This will be executed when the document unloads...
    console.log('MainView unmounted');
  }

  get presences() { return this._presences; }
  set presences(p) {this._presences = p; }

  render(presences, diff) {
    console.log("MainView.render()");
    let userList = document.getElementById("UserList");
    let userCounter = document.getElementById("UserCount");

    if (userList) {
      userList.innerHTML = Presence.list(presences, listBy)
        .map(presence => `
      <li>
        <b>${presence.user}</b>
        <br><small>online since ${presence.onlineAt}</small>
      </li>
    `)
        .join("");
    }

    if (userCounter) {
      userCounter.innerHTML = Presence.list(presences).length;
    }
  }

}


MainView._presences = {};
