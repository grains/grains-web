import MainView from "./main";

import {Socket, Presence} from "phoenix";

import socket from "../socket";
import BpmPanel from "../bpm.js";
import PresenceSketch from "../presencesketch";
import MixerPanel from "../mixerpanel";
import Counter from "../counter";




export default class View extends MainView {
  mount() {
    super.mount();


    BpmPanel.init(socket, "bpmPanel");
    PresenceSketch.init(socket, "sketch", window.innerWidth-300, window.innerHeight);
    MixerPanel.init(socket, "mixer", ["circle", "square", "triangle", "star"], PresenceSketch);
    Counter.init("counter");



    if (document.getElementById('sketch')) {

      let visuals = socket.channel("visuals:data", {});

      visuals.on("velocity", state => {
       // console.log("velocity: ", state);
        PresenceSketch.set_velocity(state.user, state.dy, state.dx);
        Counter.set_nr_messages(state.msgs);
      });

      visuals.on("acceleration", state => {
       // console.log("acceleration: ", state);
        PresenceSketch.set_acceleration(state.user, state.force);
      });

      visuals.join().receive("ok", resp => {
        console.log("visual joined");
      })
        .receive("error", reason => console.log("visual join failed", reason));

    }


    console.log("ConductorIndexView mounted");
  }

  unmount() {
    super.unmount();
    Mixer.unmount();
    console.log("ConductorIndexView unmounted");
  }

  render(presences, diff) {
    console.log("ConductorIndexView.render()");
    super.render(presences, diff);

    if (diff) {
      PresenceSketch.addBoids(diff.joins);
      PresenceSketch.removeBoids(diff.leaves);
    } else {
      PresenceSketch.syncBoids(this._presences);
    }
  }

}
