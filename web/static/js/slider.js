import Interface from "interface.js";

let SliderPanel = {

    sliderChannel : null,

    sliders: {},

    init(socket, domId, sliders) {
        if (!document.getElementById(domId)) {
            console.log("No element found: " + domId);
            return;
        }


        let that = this;
        socket.connect();
        this.sliderChannel = socket.channel("slider:" + domId);

        this.sliderChannel.join()
            .receive("ok", resp => {
                resp.values.forEach( (element) => {
                    this.setSliderValue(element);
                } );
            })
            .receive("error", reason => console.log("join failed", reason));


        let a = new Interface.Panel({
            container:document.getElementById(domId)
        });

        let nrSliders = sliders.length;

        sliders.forEach( (s, idx) => {
            this.sliders[s] = new Interface.Slider({
                bounds: [(1/nrSliders) * idx, .1, 1 / nrSliders, .5],
                label: s,

              onvaluechange: function() {
                that.send_value(s, this.value);
              }
            });
            console.log(s);
            a.add(this.sliders[s]);
        });



        a.background = 'black';
        console.log("initialized slider");
    },

    send_value(slider, value) {
        let payload = {slider: slider, value: value};
        this.sliderChannel.push("slider", payload)
                          .receive("error", e => console.log(e));

    },


};

export default SliderPanel;
