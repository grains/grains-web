import p5 from "p5";
import _ from "underscore";
import OSC from "osc-js";



import {Socket, Presence} from "phoenix";

// Boid class
// Methods for Separation, Cohesion, Alignment added
function Boid(p, x, y, type, hue, saturation, name) {
  this.p = p;
  this.name = name;
  this.acceleration = this.p.createVector(0, 0);
  this.velocity = p5.Vector.random2D();
  this.position = this.p.createVector(x, y);
  this.r = 3.0;
  this.norm_radius = 24;
  this.radius = 24;
  this.r_rand = 1.0;
  this.maxspeed = 3.2;    // Maximum speed
  this.maxforce = 0.05; // Maximum steering force
  this.type = type;
  this.separation_f = 2.0;
  this.alignment_f = 1.0;
  this.cohesion_f = 2.0;
  this.spin = parseInt(this.p.random(30, 100));
  if (this.p.random() > 0.5) { this.spin = -this.spin; };
  this.saturation = saturation || this.p.random(50, 100);
  this.color = hue || this.p.random(255); // this.p.color(this.p.random(359), 100, 100);
  this.activation = 0.5;
  this.agitation = 0.5;
  this.recovery = 0.05;
  this.volume = 1;
};



Boid.prototype.set_velocity = function(dx, dy) {
  // this.velocity = this.p.createVector(dx, dy);
  let acc = this.p.createVector(dy, dx);
  this.applyForce(acc);
};


Boid.prototype.set_acceleration = function(z) {
  if (Math.abs(z) > this.activation) {
    this.agitation = 1;
  }
};


Boid.prototype.run = function(boids) {

  this.flock(boids);
  this.update();
  this.agitate();
  this.borders();
  this.render();
};

// Forces go into acceleration
Boid.prototype.applyForce = function(force) {
  this.acceleration.add(force);
};

// We accumulate a new acceleration each time based on three rules
Boid.prototype.flock = function(boids) {
  var sep = this.separate(boids); // Separation
  var ali = this.align(boids);    // Alignment
  var coh = this.cohesion(boids); // Cohesion
  // Arbitrarily weight these forces
  sep.mult(this.separation_f);
  ali.mult(this.alignment_f);
  coh.mult(this.cohesion_f);
  // Add the force vectors to acceleration
  this.applyForce(sep);
  this.applyForce(ali);
  this.applyForce(coh);
};

// Method to update location
Boid.prototype.update = function() {      // Update velocity
  this.velocity.add(this.acceleration);
  // Limit speed
  this.velocity.limit(this.maxspeed);
  this.position.add(this.velocity);
  // Reset acceleration to 0 each cycle
  this.acceleration.mult(0);
};

// A method that calculates and applies a steering force towards a target
// STEER = DESIRED MINUS VELOCITY
Boid.prototype.seek = function(target) {
  var desired = p5.Vector.sub(target, this.position); // A vector pointing from the location to the target
  // Normalize desired and scale to maximum speed
  desired.normalize();
  desired.mult(this.maxspeed);
  // Steering = Desired minus Velocity
  var steer = p5.Vector.sub(desired, this.velocity);
  steer.limit(this.maxforce); // Limit to maximum steering force
  return steer;
};


// Wraparound
Boid.prototype.borders = function() {
  if (this.position.x < -this.radius)  this.position.x = PresenceSketch.width  + this.radius;
  if (this.position.y < -this.radius)  this.position.y = PresenceSketch.height + this.radius;
  if (this.position.x > PresenceSketch.width +this.radius) this.position.x = -this.radius;
  if (this.position.y > PresenceSketch.height+this.radius) this.position.y = -this.radius;

  // if (this.position.x < this.radius) this.velocity.x = -this.velocity.x;

  // if (this.position.y < this.radius) this.velocity.y = -this.velocity.y;
  // if (this.position.x > PresenceSketch.width - this.radius) this.velocity.x = -this.velocity.x;
  // if (this.position.y > PresenceSketch.height - this.radius) this.velocity.y = -this.velocity.y;
};

// Separation
// Method checks for nearby boids and steers away
Boid.prototype.separate = function(boids) {
  let that = this;
  var desiredseparation = 70.0;
  var steer = this.p.createVector(0, 0);
  var count = 0;
  // For every boid in the system, check if it's too close
  _.each(boids, function(boid) {
    if (that.type == boid.type) {
      var d = p5.Vector.dist(that.position, boid.position);
      // If the distance is greater than 0 and less than an arbitrary amount (0 when you are yourself)
      if ((d > 0) && (d < desiredseparation)) {
        // Calculate vector pointing away from neighbor
        var diff = p5.Vector.sub(that.position, boid.position);
        diff.normalize();
        diff.div(d); // Weight by distance
        steer.add(diff);
        count++; // Keep track of how many
      }
    }
  });
  // Average -- divide by how many
  if (count > 0) {
    steer.div(count);
  }

  // As long as the vector is greater than 0
  if (steer.mag() > 0) {
    // Implement Reynolds: Steering = Desired - Velocity
    steer.normalize();
    steer.mult(this.maxspeed);
    steer.sub(this.velocity);
    steer.limit(this.maxforce);
  }
  return steer;
};

// Alignment
// For every nearby boid in the system, calculate the average velocity
Boid.prototype.align = function(boids) {
  let that = this;
  var neighbordist = 50;
  var sum = this.p.createVector(0, 0);
  var count = 0;
  _.each(boids, function(boid) {
    if (that.type == boid.type) {
      var d = p5.Vector.dist(that.position, boid.position);
      if ((d > 0) && (d < neighbordist)) {
        sum.add(boid.velocity);
        count++;
      }
    }
  });
  if (count > 0) {
    sum.div(count);
    sum.normalize();
    sum.mult(this.maxspeed);
    var steer = p5.Vector.sub(sum, this.velocity);
    steer.limit(this.maxforce);
    return steer;
  } else {
    return this.p.createVector(0, 0);
  }
};

// Cohesion
// For the average location (i.e. center) of all nearby boids, calculate steering vector towards that location
Boid.prototype.cohesion = function(boids) {
  let that = this;
  var neighbordist = 250;
  var sum = this.p.createVector(0, 0); // Start with empty vector to accumulate all locations
  var count = 0;
  _.each(boids, function(boid) {
    if (that.type == boid.type) {
      var d = p5.Vector.dist(that.position, boid.position);
      if ((d > 0) && (d < neighbordist)) {
        sum.add(boid.position); // Add location
        count++;
      }
    }
  });
  if (count > 0) {
    sum.div(count);
    return this.seek(sum); // Steer towards the location
  } else {
    return this.p.createVector(0, 0);
  }
};

Boid.prototype.star = function (x, y, radius1, radius2, npoints) {
  var angle = this.p.TWO_PI / npoints;
  var halfAngle = angle/2.0;
  this.p.beginShape();
  for (var a = 0; a < this.p.TWO_PI; a += angle) {
    var sx = x + this.p.cos(a) * radius2;
    var sy = y + this.p.sin(a) * radius2;
    this.p.vertex(sx, sy);
    sx = x + this.p.cos(a+halfAngle) * radius1;
    sy = y + this.p.sin(a+halfAngle) * radius1;
    this.p.vertex(sx, sy);
  }
  this.p.endShape(this.p.CLOSE);
}

Boid.prototype.polygon = function(x, y, radius, npoints) {
  var angle = this.p.TWO_PI / npoints;
  this.p.beginShape();
  for (var a = 0; a < this.p.TWO_PI; a += angle) {
    var sx = x + this.p.cos(a) * radius;
    var sy = y + this.p.sin(a) * radius;
    this.p.vertex(sx, sy);
  }
  this.p.endShape(this.p.CLOSE);
}

Boid.prototype.render_circle = function() {

 // this.p.arc(0, 0, this.radius- 2, this.radius-2, 0, this.p.QUARTER_PI);
  this.p.ellipse(0, 0, 1.7 * this.radius, 2 * this.radius);
}


Boid.prototype.render_square = function() {
  this.polygon(0, 0, this.radius, 4);
}

Boid.prototype.render_triangle = function() {
  this.polygon(0, 0, this.radius, 3);
}

Boid.prototype.render_star = function() {
  this.star(0, 0, this.radius, this.radius / 2, 5);
}

// For each instrument, there is a master class that has a number of boids
// associated with it
// The following types are supported:
// - circle
// - square
// - triangle
// - star

function Master(p, x, y, type) {
  Boid.call(this, p, x, y, null);
  this.master = true;
  this.radius = 48;
  this.norm_radius = 48;
  this.type = type;
  this.maxspeed = 2.5;
  this.cohesion_f = 2;
  this.separation_f = 1;
  this.alignment_f = 2;
  this.volume = 1.0;
  this.absolute = false;

}

Master.prototype = Object.create(Boid.prototype);
Master.prototype.constructor = Master;

Master.prototype.run = function(boids) {
  let that = this;
  let list = _.filter(boids, function(boid) { return that.type == boid.type; });


  this.flock(list);
  this.update();
  this.borders();
  this.agitate(list);
  this.render(list);
  this.send_osc();

}

Master.prototype.send_osc = function() {
  let v = [0,0];
  if (this.absolute) {

    v[0] = (this.position.x * PresenceSketch.scale_x) + 3.0;
    v[1] = (this.position.y * PresenceSketch.scale_y) + 3.0;

  } else {
    v = this.velocity.array();
  }
  if (v[0] < -3.1) v[0] = -3.1;
  if (v[0] > 3.1) v[0] = 3.1;

  if (v[1] < -3.1) v[1] = -3.1;
  if (v[1] > 3.1) v[1] = 3.1;
  //let v = this.position.array();
  var message = new OSC.Message('/webosc/gyro/' + this.type, v[0], v[1], this.agitation, this.volume );

  if ((this.p.frameCount % 30.0) == 0) {
    if (PresenceSketch.osc.status() === OSC.STATUS.IS_OPEN) {
      PresenceSketch.osc.send(message);
    }
  }

}


Master.prototype.set_volume = function(vol) {
  this.volume = vol;
}

Master.prototype.set_absolute = function(flag) {
  this.absolute = (flag === 1) ? true : false;
}

Boid.prototype.agitate = function(list) {
  if (this.agitation >= this.activation) {
    this.agitation -= this.recovery;
  }

  if (list) {
    let len = 0;
    let agitation = 0;
    _.each(list, function(boid) {
      if (boid.range > 0) {
        len += 1;
        agitation += boid.agitation;
      }
    }
          );
    agitation = agitation / len;
    if (agitation > this.activation) {
      this.agitation = 1;
    }
  };

  this.radius = 2 * this.norm_radius * this.agitation;
}



Boid.prototype.render = function(list) {
  let border = 1;
  this.p.push();
  // the master looks at his minions and draws lines
  if (list) {
    let that = this;
    let neighbordist = 250;
    _.each(list, function(boid) {
      var d = p5.Vector.dist(that.position, boid.position);
      if ((d > 0) && (d < neighbordist)) {
        boid.range = d;
        that.p.stroke(255 - d);
        let weight = (255 - d) / 50;
        border = Math.max(border, weight);
        that.p.strokeWeight(weight);
        that.p.line(that.position.x, that.position.y,
                    boid.position.x, boid.position.y);
      } else
      {
        boid.range = 0;
      }
    });

  }
  this.p.pop();

  this.p.fill(this.color, this.saturation, 30 + (this.volume * 70));

  this.p.stroke(200);
  this.p.strokeWeight(border);

  this.p.push();
  this.p.translate(this.position.x, this.position.y);
  this.p.rotate(this.p.frameCount / this.spin);
  switch (this.type) {
  case 'square': this.render_square();
    break;
  case 'circle': this.render_circle();
    break;
  case 'triangle': this.render_triangle();
    break;
  case 'star': this.render_star();
    break;
  default: this.render_circle();
    break;
  }

  if (this.name) {
    this.p.rotate(-this.p.frameCount / this.spin);
    this.p.translate(-4, 4);
    this.p.textSize(18).textStyle(this.p.BOLD).strokeWeight(2).stroke(200).fill(0);
    this.p.text(this.name, 0, 0);
  }
  this.p.pop();

}




let PresenceSketch = {

  width: 960,
  height: 750,
  scale_x: 1,
  scale_y: 1,
  boids: {},
  p5: null,
  presences: {},
  osc: {},

  masters: {}, // holds the four main instruments

  types: {0: 'circle', 1: 'square', 2: 'triangle', 3: 'star'},

  set_volume(type, vol) {
    console.log("set_vol", type, vol);
    let master = PresenceSketch.masters[type];
    if (master) {
      master.set_volume(vol);
    }
  },

  set_absolute(type, abs_flag) {
    console.log("set_absolute", type, abs_flag);
    let master = PresenceSketch.masters[type];
    if (master) {
      master.set_absolute(abs_flag);
    }
  },

  set_velocity(name, dx, dy) {
    let boid = PresenceSketch.boids[name];
    if (boid) {
      boid.set_velocity(dx, dy);
    }
  },

  set_acceleration(name, z) {
    PresenceSketch.boids[name].set_acceleration(z);
  },

  addBoids(joins) {
    if (!PresenceSketch.p5) { return; };

    _.each(joins, function(val, name) {
      if (name) {
        PresenceSketch.boids[name] = new Boid(PresenceSketch.p5,
                                              Math.random() * PresenceSketch.width,
                                              Math.random() * PresenceSketch.height,
                                              PresenceSketch.types[val.type],
                                              val.hue, val.saturation, val.name);
      }
    });
  },

  removeBoids(leaves) {
    if (!PresenceSketch.p5) { return; };

    _.each(leaves, function(val, name) {
      delete PresenceSketch.boids[name];
    });
  },

  syncBoids(currentActive) {
    if (!PresenceSketch.p5) { return; };
    _.each(currentActive, function(p) {
      if (PresenceSketch.boids[p.user]) {
      } else
      {
        if (p.user) {
          PresenceSketch.boids[p.user] = new Boid(PresenceSketch.p5,
                                                  Math.random() * PresenceSketch.width,
                                                  Math.random() * PresenceSketch.height,
                                                  PresenceSketch.types[p.type],
                                                  val.hue, val.saturation, val.name);
        }
      }
    });
  },


  sketch(s) {
    PresenceSketch.p5 = s;
    var X_AXIS = 1;
    var Y_AXIS = 2;

    s.setup = function() {
      s.createCanvas(PresenceSketch.width, PresenceSketch.height);
      s.colorMode(s.HSB);
      s.background(0);

      PresenceSketch.masters['square'] = new Master(PresenceSketch.p5,
                                                    100, 100, 'square');

      PresenceSketch.masters['circle'] = new Master(PresenceSketch.p5,
                                                    PresenceSketch.width - 100,
                                                    100, 'circle');

      PresenceSketch.masters['triangle'] = new Master(PresenceSketch.p5,
                                                      100,
                                                      PresenceSketch.height - 100, 'triangle');

      PresenceSketch.masters['star'] = new Master(PresenceSketch.p5,
                                                  PresenceSketch.width - 100,
                                                  PresenceSketch.height - 100, 'star');

      let b1 = s.color(88, 131, 120);
      let b2 = s.color(0);
      s.setGradient(0, 0, PresenceSketch.width/2, PresenceSketch.height,
                    b1, b2, X_AXIS);
      s.setGradient(PresenceSketch.width/2, 0, PresenceSketch.width/2, PresenceSketch.height,
                    b2, b1, X_AXIS);
    };

    s.setGradient = function(x, y, w, h, c1, c2, axis) {

      s.noFill();

      if (axis == Y_AXIS) {  // Top to bottom gradient
        for (var i = y; i <= y+h; i++) {
          var inter = s.map(i, y, y+h, 0, 1);
          var c = s.lerpColor(c1, c2, inter);
          s.stroke(c);
          s.line(x, i, x+w, i);
        }
      }
      else if (axis == X_AXIS) {  // Left to right gradient
        for (var i = x; i <= x+w; i++) {
          var inter = s.map(i, x, x+w, 0, 1);
          var c = s.lerpColor(c1, c2, inter);
          s.stroke(c);
          s.line(i, y, i, y+h);
        }
      }
    };

    s.draw = function() {
      s.background(0);
       // Run all the boids

      _.each(PresenceSketch.masters, function(master, key, list) {
        master.run(PresenceSketch.boids);
      });
      _.each(PresenceSketch.boids, function(boid, key, list) {
        boid.run(list);
      });


      let fps = s.frameRate();
      s.fill(255);
      s.stroke(0);
      s.text("FPS: " + fps.toFixed(2), 10, s.height - 10);
    };

  },

  listBy(user, {metas: metas}) {
    return {
      user: user,
      onlineAt: metas[0].online_at
    };
  },


  init(socket, domId, w, h) {

    if (!document.getElementById(domId)) {
      return;
    }
    PresenceSketch.width = w;
    PresenceSketch.height = h;

    PresenceSketch.scale_x = -6.0 / w;
    PresenceSketch.scale_y = -6.0 / h;

    PresenceSketch.osc = new OSC();
    PresenceSketch.osc.open();  // connect to ws://localhost:8080
    console.log("OSC openend");

    PresenceSketch.osc.on("open",() => {
      console.log("osc ws connection open");
    });
    PresenceSketch.osc.on("close",() => {
      console.log("osc ws connection closed");
      PresenceSketch.osc.open();  // connect to ws://localhost:8080
    });
    PresenceSketch.osc.on("error",(err) => {
      console.log("osc ws connection error", err);

    });




    new p5(this.sketch, domId);
  }

}


export default PresenceSketch;
