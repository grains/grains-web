defmodule Grains.PresenceChannel do

  use Grains.Web, :channel
  alias Grains.{Presence, CounterAgent, StatefulMap}

  def join("presence:lobby", _, socket) do
    send self(), :after_join
    {:ok, socket}
  end

  def handle_info(:after_join, socket) do
    user_uuid = socket.assigns.user_uuid
    IO.puts("handle_join: #{user_uuid}")
    Grengine.PerformanceSupervisor.start_performance(user_uuid)

    Grains.Endpoint.broadcast("osc", "presence", %{user: user_uuid,
                                                   joins: true})



    Presence.track(socket, user_uuid, %{
          online_at: :os.system_time(:milli_seconds),
                   })
    push socket, "presence_state", Presence.list(socket)
    {:noreply, socket}

  end



end
