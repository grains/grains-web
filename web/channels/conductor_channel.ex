defmodule Grains.ConductorChannel do
  use Grains.Web, :channel

  alias Grains.ValueView

  def join("conductor:" <> slider_id, _params, socket) do

    bpm = Grains.Beat.get_bpm()

    # resp = %{values: Phoenix.View.render_many({}, ValueView, "value.json") }
    {:ok, %{bpm: bpm}, socket}
  end

  def handle_in("master", params, socket) do
    IO.puts socket.assigns.user_uuid
    IO.puts "slider: #{params["slider"]} : #{params["value"]}"
    slider = params["slider"]
    value = round(params["value"])
    uuid = socket.assigns.user_uuid

    if slider == "master" do
      Grains.Beat.set_bpm(value)
      push socket, "set_bpm", %{bpm: value}
    end

    if slider == "interval" do
      push socket, "set_interval", %{interval: value}

      Grains.Endpoint.broadcast "xy:all", "interval", %{interval: value}
    end


    {:reply, :ok, socket}

  end


  def handle_in("interval", params, socket) do
    IO.puts "interval: #{params}"
    interval = params['value']
    push socket, "set_interval", %{interval: interval}

    Grains.Endpoint.broadcast "xy:all", "interval", %{interval: interval}
    {:reply, :ok, socket}
  end




end
