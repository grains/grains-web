defmodule Grains.AccelerationChannel do
  use Grains.Web, :channel

  require Logger

  def join("acceleration:" <> _orientation_id, _params, socket) do
    Logger.info("joined orientation")

    resp = %{values: []}
    {:ok, resp, socket}
  end

  def handle_in("acceleration", params, socket) do

    Grains.MsgCounterAgent.click()

    uuid = socket.assigns.user_uuid
    z = params["z"]

    Grengine.Performance.set_values(uuid, %{
          z: z
                                    } )

    Grains.Endpoint.broadcast "visuals:data", "acceleration", %{user: uuid, force: z}

    {:reply, :ok, socket}

  end



end
