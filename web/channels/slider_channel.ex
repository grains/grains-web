defmodule Grains.SliderChannel do
  use Grains.Web, :channel

  alias Grains.ValueView

  def join("slider:" <> slider_id, _params, socket) do

    # resp = %{values: Phoenix.View.render_many({}, ValueView, "value.json") }
    {:ok, %{values: []}, socket}
  end

  def handle_in("slider", params, socket) do
    IO.puts socket.assigns.user_uuid
    IO.puts "slider: #{params["slider"]} : #{params["value"]}"
    slider = params["slider"]
    value = params["value"]
    uuid = socket.assigns.user_uuid

    Grengine.Performance.set_values(uuid, %{slider => value })
    # Grains.Endpoint.broadcast("osc", "slider", %{user: socket.assigns.user_uuid,
    #                                              element: slider,
    #                                              value: value})


    {:reply, :ok, socket}

  end



end
