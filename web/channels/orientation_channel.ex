defmodule Grains.OrientationChannel do
  use Grains.Web, :channel

  require Logger

  alias Grains.StatefulMap
  alias Grains.ValueView

  def join("orientation:" <> _orientation_id, _params, socket) do
    Logger.info("joined orientation")
    values = StatefulMap.values()

    Logger.info(inspect(values))
    resp = %{values: []}
    {:ok, resp, socket}
  end

  def handle_in("orientation", params, socket) do
    Logger.info("orientations #{inspect(params)}")

    uuid = socket.assigns.user_uuid
    pitch   = params["pitch"]
    roll    = params["roll"]

    Grengine.Performance.set_values(uuid, %{
          pitch: pitch,
          roll: roll,
                                    }
    )

    Grains.Endpoint.broadcast "visuals:data", "velocity", %{user: uuid, dx: pitch, dy: roll}

    # Grains.Endpoint.broadcast("osc", "orientation", %{user: socket.assigns.user_uuid,
    #                                                   pitch: pitch,
    #                                                   roll: roll,
    #                                                   yaw: yaw})

    {:reply, :ok, socket}

  end



end
