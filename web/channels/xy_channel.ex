defmodule Grains.XyChannel do
  use Grains.Web, :channel

  require Logger

  alias Grains.{StatefulMap, MsgCounterAgent, Type}


  def join("xy:" <> _id, _params, socket) do
    Logger.info("joined xy")

    uuid = socket.assigns.user_uuid
    nr = StatefulMap.get(uuid)
    shape = Type.select_type(uuid)
    resp = %{dot_id: nr, shape: shape}
    {:ok, resp, socket}
  end

  def handle_in("xy", params, socket) do
    nr = MsgCounterAgent.click()
    uuid = socket.assigns.user_uuid
    x   = params["x"]
    y   = params["y"]

    Grengine.Performance.set_values(uuid, %{
          x: x,
          y: y,
                                    }
    )

    Grains.Endpoint.broadcast "visuals:data", "velocity", %{user: uuid, dx: x, dy: y, msgs: nr}


    {:reply, :ok, socket}

  end



end
