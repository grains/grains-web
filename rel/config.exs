# Import all plugins from `rel/plugins`
# They can then be used by adding `plugin MyPlugin` to
# either an environment, or release definition, where
# `MyPlugin` is the name of the plugin module.
Path.join(["rel", "plugins", "*.exs"])
|> Path.wildcard()
|> Enum.map(&Code.eval_file(&1))

use Mix.Releases.Config,
    # This sets the default release built by `mix release`
    default_release: :default,
    # This sets the default environment used by `mix release`
    default_environment: Mix.env()

# For a full list of config options for both releases
# and environments, visit https://hexdocs.pm/distillery/configuration.html


# You may define one or more environments in this file,
# an environment's settings will override those of a release
# when building in that environment, this combination of release
# and environment configuration is called a profile

environment :dev do
  set dev_mode: true
  set include_erts: false
  set cookie: :"v=nEtP^Lr&GVK:!,TBl@=,%O.aCV91DOsXyx@<x[K4_O8qNo9lgEwiD.Z)pII;|:"
end

environment :prod do
  set include_erts: true
  set include_src: false
  set cookie: :"pL^K8{sPo<}Y$1?v:a()JXB%N$Xwh$S=PI|^fiU1}xX;%1P>fUb@P.%oB`^%l0A}"
end



# You may define one or more releases in this file.
# If you have not set a default release, or selected one
# when running `mix release`, the first release in the file
# will be used by default

release :grains do
  set version: current_version(:grains)
end
