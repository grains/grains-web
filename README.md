# Grains

To start your Phoenix app:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.create && mix ecto.migrate`
  * Install Node.js dependencies with `npm install`
  * Start Phoenix endpoint with `mix phoenix.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.


# Deploy

`git push production master`

Will push to `grains.switch.ch` where (Gatling)[https://github.com/hashrocket/gatling] will take care of deploying the
app.

Make sure to update the version number in `mix.exs` so that the changes are deployed
